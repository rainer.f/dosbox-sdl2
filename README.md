# *** not required anymore, usee dosbox-staging ***

# dosbox-sdl2

Modified dosbox-sdl2 package based on https://aur.archlinux.org/packages/dosbox-sdl2/

- Use sdl2_sound-hg instead of sdl_sound-hg
- Apply only SDL2 sound patch from https://github.com/Glog78/dosbox-sdl2